Symfony 2 Vagrant Development setup
=========
---

This setup is based on Ubuntu Trusty 64 bit.

Guest ip: 192.168.101.101

Host folder ../ is synced with guest /var/www/symfony/

Setup provides following components:

 - sqlite
 - redis
 - mysql
 - mongo
 - rabbitmq
 - beanstalk
 - nginx http://192.168.101.101 => /var/www/symfony/web
 - nodejs
 - php5.6
 - composer
 - phpmyadmin http://192.168.101.101:8080/phpmyadmin (login:root with no pass) 

Host folder ../ is synced with guest /var/www/symfony/


Requirements
------------

This template requires that you have installed:

- [Vagrant](http://vagrantup.com) (tested on 1.6.5)
- [Ansible](http://www.ansibleworks.com/docs/gettingstarted.html) (tested on 1.7.1)


Installation
--------------

```sh
curl -s https://getcomposer.org/installer | php
php composer.phar create-project symfony/framework-standard-edition symfony '2.5.*'
cd symfony
git clone git@bitbucket.org:telford-rowley/vm-dev.git vm
cd vm
vagrant up
```


License
----

MIT
