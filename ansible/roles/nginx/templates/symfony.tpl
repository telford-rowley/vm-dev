server {
    listen 80;

    server_name symfony;
    root        {{ symfony_doc_root }};

    error_log   /var/log/nginx/symfony_error.log;
    access_log  /var/log/nginx/symfony_netsaccess.log;

    rewrite     ^/(app|app_dev)\.php/?(.*)$ /$1 permanent;

    location / {
        index       app_dev.php;
        try_files   $uri @rewriteapp;
    }

    location @rewriteapp {
        rewrite     ^(.*)$ /app_dev.php/$1 last;
    }

    location ~ \.php($|/) {
        fastcgi_pass            unix:/var/run/php5-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include                 fastcgi_params;
        fastcgi_param           SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param           HTTPS           off;
    }
}
