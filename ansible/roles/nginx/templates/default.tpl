server {
    listen 8080;

    server_name default;
    root        {{ default_doc_root }};

    error_log   /var/log/nginx/default_error.log;
    access_log  /var/log/nginx/default_access.log;

    location / {
        try_files $uri $uri/ /index.php;
    }

    error_page 404 /404.html;
    error_page 500 502 503 504 /50x.html; location = /50x.html {
        root /usr/share/nginx/www;
    }

    location ~ \.php($|/) {
        fastcgi_pass            unix:/var/run/php5-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include                 fastcgi_params;
        fastcgi_param           SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param           HTTPS           off;
    }
}
